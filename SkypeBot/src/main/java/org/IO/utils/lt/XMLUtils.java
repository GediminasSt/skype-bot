package org.IO.utils.lt;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.command.lt.CommandList;
import org.command.lt.GlobalCommandListHandler;
/**
 * Make this class more generic godmanmit
 * 
 *
 *
 */
public class XMLUtils {
	public final static String DEFAULT_PATH = "commandList.xml";
	private static String filePath;

	public static Object unmarshalFromFile(String filePath,Class<?> clazz) {
		setXMLfilepath(filePath);
		try {
			JAXBContext context = JAXBContext.newInstance(clazz);
			Unmarshaller un = context.createUnmarshaller();
			Object emp = un.unmarshal(new File(filePath));
			System.out.println("Readed from xml");
			return emp;
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Object unmarshalString(String string,Class<?> clazz){
		try {
			JAXBContext context = JAXBContext.newInstance(clazz);
			Unmarshaller un = context.createUnmarshaller();
			return (string != null) ? un.unmarshal(new StringReader(string)) : null;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String marshalCommandListString(Object Object){
		try {
			JAXBContext context = JAXBContext.newInstance(Object.getClass());
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter stringWritter = new StringWriter();
			m.marshal(Object, stringWritter);
			return stringWritter.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void saveCommandList() {
		try {
			if (filePath == null)
				filePath = DEFAULT_PATH;
			if (!GlobalCommandListHandler.getCommandMap().isEmpty()) {
				JAXBContext context = JAXBContext.newInstance(CommandList.class);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				m.marshal(GlobalCommandListHandler.getStaticCommandListInstanceToSave(), new File(filePath));
				System.out.println("Saved to XML");
			}else{
				System.out.println("No commands to save fella!");
			}
		} catch (JAXBException e) {
			e.printStackTrace();
		}	
	}
	
	public static void readSavedCommandFileIfExist(){
		if(new File(DEFAULT_PATH).exists())
			GlobalCommandListHandler.setStaticComandList((CommandList) unmarshalFromFile(DEFAULT_PATH,CommandList.class));
	}
	
	public static String getXMLfilepath(){
		return filePath;
	}
	
	public static void setXMLfilepath(String aFilepath){
		filePath = aFilepath;
	}
}
