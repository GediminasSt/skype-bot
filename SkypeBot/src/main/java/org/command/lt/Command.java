package org.command.lt;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Command {
	
	private String commandText;
	private String answerText;
	private boolean isExact;
	private boolean isPrivate;
	
	public Command() {
		//leaving default constructor
	}
	
	public Command(String aCommandText,String aAnswerText, boolean IsExact, boolean IsPrivate) {
		this.commandText = aCommandText;
		this.answerText = aAnswerText;
		this.isExact = IsExact;
		this.isPrivate = IsPrivate;
	}
	
	public boolean isPrivate() {
		return isPrivate;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public void setCommandText(String commandText) {
		this.commandText = commandText;
	}
	
	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}
	
	public void setExact(boolean isExact) {
		this.isExact = isExact;
	}
		
	public String getCommandText() {
		return commandText;
	} 
	
	public String getAnswerText() {
		return answerText;
	}
		
	public boolean isExact() {
		return isExact;
	}
}
