package org.command.lt;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CommandList {
	
	private List<Command> commandList;	
	
	@XmlElement(name="Command")
    public List<Command> getCommandList() {
        return commandList;
    }
	
	 public void setCommandList(List<Command> commandList) {
		 this.commandList = commandList;
	 }
	 
	 public void addCommand(Command command){
		 if(commandList == null){
			 commandList = new ArrayList<Command>();
		 }
		 commandList.add(command);
	 }
}
