package org.command.lt;

import java.util.HashMap;
import java.util.Map;
/**
 * 
 * Puts single CommandList map that is not catecorized
 *
 */
public class GlobalCommandListHandler {
	private static final Map<String,Command> commandMap = new HashMap<String,Command>();
	private static CommandList commandListStaticInstance = new CommandList();
	
	public static void setStaticComandList(CommandList list) {
		commandListStaticInstance = list;
		setupCommandMap();
	}
		
	public static CommandList getStaticCommandListInstance(){
		return commandListStaticInstance;
	}
	
	public static CommandList getStaticCommandListInstanceToSave(){
		CommandList commandList = new CommandList();
		for(String key: commandMap.keySet()){
			commandList.addCommand(commandMap.get(key));
			System.out.println("Renewing Command: " + key);
		}
		commandListStaticInstance = commandList;
		return commandListStaticInstance;
	}
	
	private static void setupCommandMap(){
		 for(Command command : commandListStaticInstance.getCommandList()){
			 commandMap.put(command.getCommandText(), command);
			 System.out.println(command.getCommandText() + " Has put onto Map");
		 }
	 }
	 
	 public static Map<String,Command> getCommandMap(){
		 return commandMap;
	 }
	 
	 public static void updateCommandMap(Command command){
		 commandMap.put(command.getCommandText(), command);
	 }
	 
	 public static void removeFromCommandMap(String key){
		 commandMap.remove(key);
	 }
}
