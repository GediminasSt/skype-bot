package org.command.lt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.IO.utils.lt.XMLUtils;

import service.consumer.ConsumerImpl;

/**
 * 
 * Class for handling Individual chatters command list
 *
 */
public class IndividualCommandListHandler {
	
	private static CommandList getIndividualCommandList(String chattersId){
		ConsumerImpl consumerImpl = new ConsumerImpl();
		CommandList commandList;
		String string = (String) consumerImpl.receive(chattersId);
		if(null == string){
			
			commandList = new CommandList();
			commandList.addCommand(SystemCommands.MY_ID.setPublicCommand(chattersId));
			consumerImpl.update(chattersId, commandList);
			addSystemCommands(commandList);
		}else{			
			// TODO this shit is fucking performance hog.. in the future find a solution to it 
			commandList = (CommandList) XMLUtils.unmarshalString(string, CommandList.class);
			addSystemCommands(commandList);
		}
		return commandList;
	}
	
	private static void addSystemCommands(CommandList commandList){
		
		List<String> allCommandKeys = new ArrayList<String>();
		StringBuilder buildedString = new StringBuilder();
		buildedString.append("Your commands:\n");
		Arrays.stream(SystemCommands.values()).forEach(key -> allCommandKeys.add(key.getKey() + "\n"));
		Arrays.stream(GlobalCommandListHandler.getCommandMap().keySet().toArray()).forEach(key -> allCommandKeys.add(key.toString()+ "\n"));
		allCommandKeys.forEach(key -> buildedString.append(key));
		
		commandList.addCommand(SystemCommands.COMMAND_LIST.setPublicCommand(buildedString.toString()));
	}
	
	//simply - commandList to map stuff
	private static Map<String,Command> getIndividualCommandMap(CommandList commandList){
		Map<String,Command> commandMap = new HashMap<String, Command>();
		for(Command command : commandList.getCommandList()){
			 commandMap.put(command.getCommandText(), command);
		}
		return commandMap;
	}
	
	public static Command getCommand(String chattersId, String commandString){
		//TODO performance hog no2
		Map<String,Command> commandMapIndividual = getIndividualCommandMap(getIndividualCommandList(chattersId));
		if(commandMapIndividual.containsKey(commandString)){
			return commandMapIndividual.get(commandString);
		}else{
			return GlobalCommandListHandler.getCommandMap().get(commandString);
		}
	}
	
}
