package org.command.lt;

public enum SystemCommands {
	
	MY_ID("!myid"),
	COMMAND_LIST("!commands");
	
	private String systemCommandText;
	
	SystemCommands(String commandText){
		systemCommandText = commandText;
	}
	
	public Command setPublicCommand(String answerText){
		Command systemCommand = new Command();
		systemCommand.setCommandText(systemCommandText);
		systemCommand.setAnswerText(answerText);
		systemCommand.setExact(true);
		systemCommand.setPrivate(false);
		return systemCommand;
	}
	
	public Command setPrivateCommand(String answerText){
		Command privateCommand = setPublicCommand(answerText);
		privateCommand.setPrivate(true);
		return privateCommand;
	}
	
	public String getKey(){
		return systemCommandText;
	}
}
