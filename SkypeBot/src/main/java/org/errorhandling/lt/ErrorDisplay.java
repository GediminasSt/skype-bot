package org.errorhandling.lt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.gui.lt.ErrorWindowGUI;
import org.programa.lt.ShellwindowHandlerImpl;

public class ErrorDisplay extends ShellwindowHandlerImpl {


	@Override
	public void onCreate() {
		setupGUI(new ErrorWindowGUI(getShell(), SWT.NONE));
		super.onCreate();
	}

	public static void displayError(String errorMessage) {
		errorInstance(errorMessage);
	}
	
	public void threadedDisplayError(String errorMessage){
		Display.getDefault().asyncExec(new Runnable() {			
			@Override
			public void run() {
				errorInstance(errorMessage);
			}
		});
	}
	
	private static void errorInstance(String errorMessage){
		ErrorDisplay errDisp = new ErrorDisplay();
		errDisp.setupShell("Error", 300,100);
		Label errorLabel = (Label) errDisp.getGUI().getObject("errorMessage");
		errorLabel.setText(errorMessage);
		errDisp.getShell().open();
	}
}
