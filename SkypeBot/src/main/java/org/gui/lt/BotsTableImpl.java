package org.gui.lt;

import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

public abstract class BotsTableImpl implements BotsTable {
	public static Table table;
	
	@Override
	public void refresTableContents() {
		table.clearAll();
		for(TableItem tableItem :table.getItems()){
			tableItem.dispose();
		}		
	}
	
	public Table getTable(){
		return table;
	}

}
