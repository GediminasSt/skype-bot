package org.gui.lt;

import org.command.lt.Command;
import org.command.lt.GlobalCommandListHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.wb.swt.SWTResourceManager;
import org.properties.lt.PropertiesGUI;

public class CommandEditorGUI extends Composite implements GUI{

	private boolean isPrivateStatus = false;
	
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	
	private Text txtCommandtext;
	private Text textResponse;
	private Text txtRmcommand;
	private Button btnRemove;
	private Button btnAdd;
	private Label lblRemovecommand;
	private Label lblResponse;
	private Label lblCommand;
	private Label lblAddcommand;
	private Label lblMessage;
	private CommandListTable commandListTable;
	private ScrolledComposite commandListTableScrolledComposite;
	private Button btnIsprivatecheck;
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public CommandEditorGUI(Composite parent, int style) {
		super(parent, SWT.NONE);
		setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				toolkit.dispose();
			}
		});
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		setLayout(new GridLayout(4, false));
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
		
		commandListTableScrolledComposite = new ScrolledComposite(this, SWT.NONE);
		commandListTableScrolledComposite.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, true, 1, 9));
		toolkit.adapt(commandListTableScrolledComposite);
		toolkit.paintBordersFor(commandListTableScrolledComposite);
		commandListTable = new CommandListTable(commandListTableScrolledComposite);
		objectMap.put("commandListTable", commandListTable);
		
		new Label(this, SWT.NONE);
		
		lblCommand = new Label(this, SWT.NONE);
		toolkit.adapt(lblCommand, true, true);
		objectMap.put("lblCommand", lblCommand);
		lblCommand.setText("Command");
		
		lblResponse = new Label(this, SWT.NONE);
		objectMap.put("lblResponse", lblResponse);
		toolkit.adapt(lblResponse, true, true);
		lblResponse.setText("Response");
		
		lblAddcommand = new Label(this, SWT.NONE);
		objectMap.put("lblAddcommand", lblAddcommand);
		lblAddcommand.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		toolkit.adapt(lblAddcommand, true, true);
		lblAddcommand.setText("Add Command");
		
		txtCommandtext = new Text(this, SWT.BORDER);
		txtCommandtext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(txtCommandtext, true, true);
		
		textResponse = new Text(this, SWT.WRAP | SWT.BORDER);
		GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 5);
		gd_text.heightHint = 186;
		gd_text.widthHint = 173;
		textResponse.setLayoutData(gd_text);
		toolkit.adapt(textResponse, true, true);
		new Label(this, SWT.NONE);
		
		btnIsprivatecheck = new Button(this, SWT.CHECK);
		btnIsprivatecheck.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button isPrivateButtonCheck = (Button) e.getSource();
				isPrivateStatus = isPrivateButtonCheck.getSelection();
				System.out.println("Is command private : " + isPrivateStatus);
			}
		});
		toolkit.adapt(btnIsprivatecheck, true, true);
		btnIsprivatecheck.setText("Is command Private?");
		new Label(this, SWT.NONE);
		
		btnAdd = new Button(this, SWT.NONE);
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				Command command = new Command();
				if ((txtCommandtext.getText().length() >= PropertiesGUI.COMMAND_WORD_LENGHT)
						|| (textResponse.getText().length() >= PropertiesGUI.COMMAND_WORD_LENGHT)) {
					command.setCommandText(txtCommandtext.getText());
					command.setAnswerText(textResponse.getText());
					command.setExact(true);
					command.setPrivate(isPrivateStatus);
					GlobalCommandListHandler.updateCommandMap(command);
					txtCommandtext.setText("");
					textResponse.setText("");
					lblMessage.setText("Command " + command.getCommandText() + " added to the list");
					commandListTable.refresTableContents();
				}else{
					lblMessage.setText("Some fields contain less than 3 symbols");
				}
			}
		});
		toolkit.adapt(btnAdd, true, true);
		btnAdd.setText("Add");
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
		
		lblMessage = new Label(this, SWT.NONE);
		GridData gd_lblMessage = new GridData(SWT.LEFT, SWT.TOP, false, false, 2, 1);
		gd_lblMessage.widthHint = 213;
		gd_lblMessage.heightHint = 116;
		lblMessage.setLayoutData(gd_lblMessage);
		lblMessage.setText(" ");
		toolkit.adapt(lblMessage, true, true);
		
		lblRemovecommand = new Label(this, SWT.NONE);
		lblRemovecommand.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		toolkit.adapt(lblRemovecommand, true, true);
		lblRemovecommand.setText("Remove Command");
		
		txtRmcommand = new Text(this, SWT.BORDER);
		txtRmcommand.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(txtRmcommand, true, true);
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
		
		btnRemove = new Button(this, SWT.NONE);
		btnRemove.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				GlobalCommandListHandler.removeFromCommandMap(txtRmcommand.getText());
				lblMessage.setText(txtRmcommand.getText() + " command was removed if it existed :D");
				commandListTable.refresTableContents();
			}
		});
		toolkit.adapt(btnRemove, true, true);
		btnRemove.setText("Remove");
		new Label(this, SWT.NONE);
	}

	@Override
	public Object getObject(String controlKey) {
		return objectMap.get(controlKey);
	}
	
}
