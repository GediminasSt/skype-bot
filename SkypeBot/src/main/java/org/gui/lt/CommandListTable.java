package org.gui.lt;

import org.command.lt.GlobalCommandListHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.SWTResourceManager;

public class CommandListTable extends BotsTableImpl {
	
	private String[] collumnNames = {"Command","Response","Exact","Private"};	
	
	public CommandListTable(ScrolledComposite composite) {
		table = new Table(composite, SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		table.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
    composite.setContent(table);
    composite.setExpandHorizontal(true);
    composite.setExpandVertical(true);
    composite.setAlwaysShowScrollBars(true);
    composite.setMinSize(table.computeSize(SWT.DEFAULT, SWT.DEFAULT));
    
    TableColumn tableColumn = new TableColumn(table, SWT.NONE);
    tableColumn.setText(collumnNames[0]);
    tableColumn.setWidth(100);
    tableColumn = new TableColumn(table, SWT.NONE);
    tableColumn.setText(collumnNames[1]);
    tableColumn.setWidth(200);
    tableColumn = new TableColumn(table, SWT.NONE);
    tableColumn.setText(collumnNames[2]);
    tableColumn.setWidth(50);
    tableColumn = new TableColumn(table, SWT.NONE);
    tableColumn.setText(collumnNames[3]);
    tableColumn.setWidth(50);
	}
		
	public void refresTableContents(){
		super.refresTableContents();
		for(String command : GlobalCommandListHandler.getCommandMap().keySet()){
			new TableItem(table,SWT.NONE).setText(
					new String[]{
							GlobalCommandListHandler.getCommandMap().get(command).getCommandText(),
							GlobalCommandListHandler.getCommandMap().get(command).getAnswerText(),
							GlobalCommandListHandler.getCommandMap().get(command).isExact() ? "true" : "false",
							GlobalCommandListHandler.getCommandMap().get(command).isPrivate() ? "true" : "false"
					});		
		}
	}		
}
