package org.gui.lt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class ErrorWindowGUI extends Composite implements GUI{

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ErrorWindowGUI(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Label errorMessage = new Label(this, SWT.NONE);
		objectMap.put("errorMessage", errorMessage);
		GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_label.heightHint = 68;
		gd_label.widthHint = 285;
		errorMessage.setLayoutData(gd_label);
		errorMessage.setText("New Label");

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	@Override
	public Object getObject(String controlKey) {
		return objectMap.get(controlKey);
	}
}
