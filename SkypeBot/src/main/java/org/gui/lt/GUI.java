package org.gui.lt;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public interface  GUI {
	public Map<String,Object> objectMap = Collections.synchronizedMap(new HashMap<String,Object>());
	public Object getObject(String controlKey);
}
