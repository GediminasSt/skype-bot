package org.gui.lt;

import org.eclipse.swt.widgets.Display;

public class GUIThread {
	public static Thread getGuiThread(Runnable runnableInstance){
		return new Thread(new Runnable(){
			@Override
			public void run() {
				Display.getDefault().asyncExec(runnableInstance);
			}
		});
	}
}
