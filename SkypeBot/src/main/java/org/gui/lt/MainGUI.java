package org.gui.lt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.wb.swt.SWTResourceManager;
import org.programa.lt.CommandEditor;
import org.programa.lt.CommandEditorImpl;
import org.skype.lt.ChatHandler;

import com.skype.SkypeException;

public class MainGUI extends Composite implements GUI{
	
	private Boolean ifShowOnlyGroups = false;
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	final Label lblSelectedChat;
	final List list; 
	final Label lblPasirinkKaBotint;
	private Button btnCommandManager;
	private Label lblTools;
	private Label lblConfiguration;
	private Button btnIfshowonlygroups;;

	public MainGUI(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));
		
		objectMap.put("ifShowOnlyGroups", ifShowOnlyGroups);
		
		lblPasirinkKaBotint = new Label(this, SWT.NONE);
		lblPasirinkKaBotint.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		objectMap.put("lblPasirinkKaBotint", lblPasirinkKaBotint);
		lblPasirinkKaBotint.setText("Please turn on your Skype, and restart the app");
		
		lblSelectedChat = new Label(this, SWT.NONE);
		GridData gd_lblSelectedChat = new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1);
		gd_lblSelectedChat.widthHint = 219;
		lblSelectedChat.setLayoutData(gd_lblSelectedChat);
		objectMap.put("lblPasirinkimas", lblSelectedChat);
		lblSelectedChat.setText("Choice");
		
		list = new List(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		GridData gd_list = new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1);
		gd_list.widthHint = 234;
		list.setLayoutData(gd_list);
		objectMap.put("list", list);
		
		list.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {			
				try {
					ChatHandler.setActiveChat((String) list.getData(list.getSelection()[0]));
					lblSelectedChat.setText(ChatHandler.getActiveChat().getWindowTitle());
				} catch (SkypeException e1) {
					e1.printStackTrace();
				}			
			}
		});
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
		
		lblConfiguration = new Label(this, SWT.NONE);
		lblConfiguration.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblConfiguration.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		lblConfiguration.setText("Configuration");
		
		btnIfshowonlygroups = new Button(this, SWT.CHECK);
		btnIfshowonlygroups.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button isShowGroupsButtonCheck = (Button) e.getSource();
				ifShowOnlyGroups = isShowGroupsButtonCheck.getSelection();
				ChatHandler.populateSWTListWithChats(list,ifShowOnlyGroups);
				System.out.println("Show only groups: " + ifShowOnlyGroups);
			}
		});
		btnIfshowonlygroups.setText("Show Only Groups");
		new Label(this, SWT.NONE);
		
		lblTools = new Label(this, SWT.NONE);
		lblTools.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblTools.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		lblTools.setText("Tools");
		
		btnCommandManager = new Button(this, SWT.NONE);
		btnCommandManager.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				CommandEditor commandEditor = CommandEditorImpl.getInstance();
				commandEditor.getHandlerInstance().setupShell("Command Manager", 850, 350);
				commandEditor.getHandlerInstance().openDialogWindow();
				BotsTable botsTable = (BotsTable) commandEditor.getHandlerInstance().getGUI().getObject("commandListTable");
				botsTable.refresTableContents();
			}
		});
		btnCommandManager.setText("Command Manager");
		new Label(this, SWT.NONE);
	}
	
	@Override
	public Object getObject(String controlKey) {
		return objectMap.get(controlKey);
	}
	
}
