package org.programa.lt;

import org.IO.utils.lt.XMLUtils;
import org.eclipse.swt.SWT;
import org.gui.lt.CommandEditorGUI;

public class CommandEditorHandler extends ShellwindowHandlerImpl implements CommandEditor {
	
	private static CommandEditorHandler commandEditorHandlerInstance;
	
	@Override
	public void onCreate() {
		setupGUI(new CommandEditorGUI(getShell(),SWT.NONE));
		super.onCreate();
	}

	@Override
	public CommandEditorHandler getHandlerInstance() {
		if(commandEditorHandlerInstance == null){
			commandEditorHandlerInstance = new CommandEditorHandler();
			return commandEditorHandlerInstance;
		}else{
			return commandEditorHandlerInstance;
		}	
	}

	@Override
	public void onClose() {
		XMLUtils.saveCommandList();
		super.onClose();
	}
	
}
