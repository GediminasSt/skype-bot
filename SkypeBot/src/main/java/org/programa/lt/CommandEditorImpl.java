package org.programa.lt;

public class CommandEditorImpl extends CommandEditorHandler{
	
	private static CommandEditorImpl commandEditorImplStaticInstance;
	
	public static CommandEditorImpl getInstance(){
		if(commandEditorImplStaticInstance == null){
			commandEditorImplStaticInstance = new CommandEditorImpl();
			return commandEditorImplStaticInstance;
		}else{
			return commandEditorImplStaticInstance;
		}
	}	
}
