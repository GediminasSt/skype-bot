package org.programa.lt;

import org.eclipse.swt.SWT;
import org.gui.lt.MainGUI;


public class MainWindowHandler extends ShellwindowHandlerImpl implements MainWindow{

	private static MainWindowHandler mainWindowHandler;
	
	@Override
	public void onCreate() {
		setupGUI(new MainGUI(getShell(), SWT.NONE));
		super.onCreate();
	}

	@Override
	public MainWindowHandler getHandlerInstance() {
		if(mainWindowHandler == null){
			mainWindowHandler = new MainWindowHandler();
			return mainWindowHandler;
		}else{
			return mainWindowHandler;
		}
	}

}
