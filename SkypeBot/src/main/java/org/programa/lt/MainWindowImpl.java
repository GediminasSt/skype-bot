package org.programa.lt;

public class MainWindowImpl extends MainWindowHandler{

	public static MainWindowImpl mainWindowImplStaticInstance;

	public static MainWindowImpl getInstance() {
		if (mainWindowImplStaticInstance == null) {
			mainWindowImplStaticInstance = new MainWindowImpl();
			return mainWindowImplStaticInstance;
		} else {
			return mainWindowImplStaticInstance;
		}
	}

}
