package org.programa.lt;

import org.eclipse.swt.widgets.Shell;
import org.gui.lt.GUI;

public interface ShellwindowHandler {
		public void setupShell(String windowName,int sizeHeight,int sizeWidth);
		public void setupGUI(GUI gui);
		public void openDialogWindow();
		public void closeDialogWIndow();
		public Shell getShell();
		public GUI getGUI();
		public void onCreate();
		public void onClose();
}
