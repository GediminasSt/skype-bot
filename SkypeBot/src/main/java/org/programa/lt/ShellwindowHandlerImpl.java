package org.programa.lt;

import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Shell;
import org.gui.lt.GUI;
import org.gui.lt.MainGUI;

public class ShellwindowHandlerImpl implements ShellwindowHandler {
	
	private Shell shell = null;
	private GUI gui;
	private static Object lock = new Object();
	public ShellwindowHandlerImpl() {
		System.out.println("ShellwindowImpl created");
	}
	
	@Override
	public void setupShell(String windowName,int sizeHeight,int sizeWidth) {
		getShell().setText(windowName);
		getShell().setLayout(new RowLayout());
		getShell().setSize(sizeHeight, sizeWidth);
	}
	
	@Override
	public void openDialogWindow() {
		shell.open();
	}
	
	@Override
	public void closeDialogWIndow() {
		shell.close();
	}
	
	@Override
	public Shell getShell() {
		if(shell == null){
			synchronized(lock){
				if (shell == null)
					shell = new Shell();
				onCreate();
				System.out.println("New shell instance Created");
			}
		}
		return shell;
	}

	@Override
	public void onClose() {
		//allow Windows to dispose main window and to not create new instance of the shell when checking if it's dispossed @ main while loop
		if(!(getGUI().getClass().equals(MainGUI.class)))
			shell = null;
		gui = null;
		System.out.println("onClose() completed Shell instance disposed");
	}

	@Override
	public void onCreate() {
		//creating dispose listener on shell creation to do stuff after it closes
		System.out.println("onCreate() launched");
		getShell().addDisposeListener(new DisposeListener() {		
			@Override
			public void widgetDisposed(DisposeEvent arg0) {
				onClose();
			}
		});	
	}

	@Override
	public GUI getGUI() {		
		return gui;
	}

	@Override
	public void setupGUI(GUI gui) {
		if (this.gui == null) {
			this.gui = gui;
			System.out.println(gui.getClass());
			System.out.println("New composite set up");
		}
	}
	
}
