package org.programa.lt;

import java.util.HashSet;
import java.util.Set;

import org.IO.utils.lt.XMLUtils;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.List;
import org.skype.lt.ChatHandler;
import org.skype.lt.ReadAndPostURLRunnable;
import org.skype.lt.SetupSkype;

import com.skype.SkypeException;

public class proge {
	
	private static Set<Thread> threadSet = new HashSet<Thread>();
	/**
	 * @param args
	 * @throws SkypeException
	 */
	public static void main(String[] args) {
		System.out.println("Launching Main Method");
		//GUI
		Display display = new Display();
		MainWindow mainWindow = MainWindowImpl.getInstance();
		mainWindow.getHandlerInstance().setupShell(
				"Skype Bot", 300, 350);
		mainWindow.getHandlerInstance().getShell().open();
		//
		//Setup
		ChatHandler.populateSWTListWithChats((List) mainWindow
				.getHandlerInstance().getGUI().getObject("list"), false);
		
		SetupSkype.setup();
		
		setupBackgroundThreads();
		
		XMLUtils.readSavedCommandFileIfExist(); 
		//
		while (!mainWindow.getHandlerInstance().getShell().isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		
		interruptAllThreads();
		
		display.dispose();
	}
	
	private static void setupBackgroundThreads(){
		//url reading thread
		Thread ReadUrlthread = new Thread(new ReadAndPostURLRunnable());
		ReadUrlthread.setName("ReadUrlThread");
		ReadUrlthread.start();
		threadSet.add(ReadUrlthread);
	}
	
	private static void interruptAllThreads(){
		for(Thread thread : threadSet){
			thread.interrupt();
		}
	}
}
