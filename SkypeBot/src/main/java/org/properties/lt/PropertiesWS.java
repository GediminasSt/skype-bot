package org.properties.lt;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public enum PropertiesWS {
	//Just add properties here when needed;
	BACKEND_IP("backendip"),
	BAKCEND_PORT("backendport"),
	READ_AND_POST_URL("readandposturl"),
	READ_AND_POST_TIME("readandposttime");
	
	private String parameter;
	
	PropertiesWS(String parameterName){
		this.parameter = parameterName;
	}
	
	static private final Properties PROPERTIES = new Properties();
	static private final String PROPERTIES_FILE_NAME = "configskypebot.properties";
	static{
			readPropertiesFile();
	}
	
	private static void createPropertiesFileWithDefaultValues(){
		try {
			System.out.println("Beginning to create file");
			FileWriter fileWriter = new FileWriter(PROPERTIES_FILE_NAME,true);
			for(PropertiesWS properties : PropertiesWS.values()){
				fileWriter.write(properties.parameter + "=\n");
				System.out.println("Property: " + properties.parameter + " was written to file");
			}			
			fileWriter.close();
			readPropertiesFile();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	private static void readPropertiesFile(){
		InputStream inputStream = null;
		try{
			inputStream = new FileInputStream(PROPERTIES_FILE_NAME);		
			PROPERTIES.load(inputStream);
			if(PROPERTIES.isEmpty()){
				createPropertiesFileWithDefaultValues();
			}
		}catch(IOException e){	
			createPropertiesFileWithDefaultValues();
		}finally{
			if(inputStream !=null) {
				try{
					inputStream.close();
				} catch (IOException e){					
				}
			}
		}	
	}
	
	public String getString(){
		if(PROPERTIES.getProperty(parameter).isEmpty()){
			return null;
		}
		return PROPERTIES.getProperty(parameter);
	}
}
