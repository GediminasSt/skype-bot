package org.skype.lt;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.widgets.List;
import org.errorhandling.lt.ErrorDisplay;

import com.skype.Chat;
import com.skype.Skype;
import com.skype.SkypeException;


public final class ChatHandler {
	
	private static final Map<String,Chat> CHAT_MAP = Collections.synchronizedMap(new HashMap<String, Chat>());
	private static String activeChatId;
	
	static {
		refreshChatMap();
	}
	
	public static void populateSWTListWithChats(List list, boolean ifShowGroups){
		try {
		list.removeAll();
			
		Set<String> displayChatUniqueList = new HashSet<String>();
		for(String chatId : CHAT_MAP.keySet()){			
			if(ifShowGroups && CHAT_MAP.get(chatId).getWindowTitle().contains(",")){
				displayChatUniqueList.add(CHAT_MAP.get(chatId).getWindowTitle());
			}else if(!ifShowGroups){
				displayChatUniqueList.add(CHAT_MAP.get(chatId).getWindowTitle());
				//fill swt list with data
				list.setData(CHAT_MAP.get(chatId).getWindowTitle(), chatId);
			}
		}
		        
		for(String stringToFillList : displayChatUniqueList){
			list.add(stringToFillList);	
		}
	} catch (SkypeException e) {
		e.printStackTrace();
		}
	}
	
	public static Chat getChat(String idString){	
		return CHAT_MAP.get(idString);
	}
	
	public static void setActiveChat(String string){
		activeChatId = string;
		System.out.println("Listening to: " + activeChatId);
	}
	
	public static Chat getActiveChat(){
		return CHAT_MAP.get(activeChatId);
	}
	
	public static void refreshChatMap(){
		try {
			for(Chat chat : Skype.getAllChats()){
				CHAT_MAP.put(chat.getId(), chat);
			}
		} catch (SkypeException e) {
			System.out.println("Failed to load to skype chats");
			e.printStackTrace();
		}
	}
	
	public synchronized static void postToActiveChat(String message){
		if(getActiveChat() != null){
			try {
				getActiveChat().send(message);
				System.out.println("Message sent to: " + getActiveChat().getId());
			} catch (SkypeException e) {
				ErrorDisplay.displayError("Could not send message: " + message);
				e.printStackTrace();
			}
		}
	}
	
}
