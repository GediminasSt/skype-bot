package org.skype.lt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.errorhandling.lt.ErrorDisplay;
import org.properties.lt.PropertiesWS;

/**
 * 
 * Checks if property with url is set and checks if there's selected chat.
 * then reads the url and posts content into active chat, all of this runs on  awhile loop every x seconds(in the property)
 * if property is not set its 3 mins
 *
 */

public class ReadAndPostURLRunnable implements Runnable {
	
	public static String oldMessage = new String();
	
	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			if (PropertiesWS.READ_AND_POST_URL.getString() != null && ChatHandler.getActiveChat() != null){
				try {
					runThread();					
					} catch (NumberFormatException | IOException e) {
						e.printStackTrace();
						new ErrorDisplay().threadedDisplayError("bad readandposturl param, restart!");
						Thread.currentThread().interrupt();
					}
			}
			try {
				Thread.sleep(PropertiesWS.READ_AND_POST_TIME.getString() != null 
						? Integer.parseInt(PropertiesWS.READ_AND_POST_TIME.getString()) * 1000 : 180 * 1000);
			} catch (NumberFormatException | InterruptedException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
		}
	}
	
	
	
	private void runThread() throws MalformedURLException, IOException{
		System.out.println("Reading URL");
			BufferedReader in;
				in = new BufferedReader(new InputStreamReader(new URL(
						PropertiesWS.READ_AND_POST_URL.getString()).openStream()));
				String currentMessage = in.readLine();
				if (!currentMessage.equals(oldMessage)) {
					ChatHandler.postToActiveChat(currentMessage);
					oldMessage = currentMessage;
				}
				in.close();
		}		
}
