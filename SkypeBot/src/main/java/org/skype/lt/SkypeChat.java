package org.skype.lt;

import org.command.lt.Command;

import com.skype.Chat;

public interface SkypeChat {
	public void onPublicEvent(Command command);
	public void onPrivateEvent(Command command, Chat privateChat);
}
