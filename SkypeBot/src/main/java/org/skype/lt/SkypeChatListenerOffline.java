package org.skype.lt;

import java.util.ArrayList;
import java.util.List;

import org.command.lt.Command;
import org.command.lt.IndividualCommandListHandler;
import org.errorhandling.lt.ErrorDisplay;

import com.skype.Chat;
import com.skype.ChatListenerMananager;
import com.skype.ChatMessage;
import com.skype.SkypeException;
import com.skype.User;

public class SkypeChatListenerOffline extends ChatListenerMananager implements SkypeChat {
	
	@Override
	public void chatMessageReceived(ChatMessage chatMessage)
			throws SkypeException {
		
		if (chatMessage.getStatus().equals(ChatMessage.Status.RECEIVED)) {
			
			System.out.println(chatMessage.getSenderId()+ " " + chatMessage.getStatus());
			Command individualCommand = IndividualCommandListHandler.getCommand(chatMessage.getSenderId(), chatMessage.getContent());
			
			List<String> allActiveUserIdOfChat = new ArrayList<String>();
			
			for(User user : ChatHandler.getActiveChat().getAllActiveMembers())
				allActiveUserIdOfChat.add(user.getId());
			
			if(individualCommand != null &&	allActiveUserIdOfChat.contains(chatMessage.getSenderId())){
				if(individualCommand.isPrivate()){
					onPrivateEvent(individualCommand, chatMessage.getSender().chat());
				}else{
					onPublicEvent(individualCommand);
				}
									
			}
		super.chatMessageReceived(chatMessage);
		}
	}

	@Override
	public void onPublicEvent(Command command) {
		ChatHandler.postToActiveChat(command.getAnswerText());		
	}

	@Override
	public void onPrivateEvent(Command command, Chat privateChat) {
		try {
			privateChat.send(command.getAnswerText());
		} catch (SkypeException e) {
			ErrorDisplay.displayError("Unable to send message privatelly");
			e.printStackTrace();
		}		
	}
	
}
