package org.skype.lt;

import org.errorhandling.lt.ErrorDisplay;

import com.skype.Skype;
import com.skype.SkypeException;


public class SkypeChatListenerOfflineSetup {

	public SkypeChatListenerOfflineSetup() {
		try {
			Skype.addChatMessageListener(new SkypeChatListenerOffline());
		} catch (SkypeException e) {
			ErrorDisplay.displayError("Could not find skype that's turned on");
			e.printStackTrace();
		}
	}
	
}
