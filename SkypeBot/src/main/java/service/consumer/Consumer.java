package service.consumer;

public interface Consumer {
	public Object receive(String id);
	public void update(String id, Object Object);
}
