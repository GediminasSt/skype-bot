package service.consumer;

import java.rmi.RemoteException;

import org.IO.utils.lt.XMLUtils;
import org.properties.lt.PropertiesWS;

import servers.CommandListReceiverDataBaseProxy;
import service.consumer.cache.Cache;

/**
 * 
 * checks if property is set for backend url if its set, than stuff with webservice happens. Caching n shit
 *
 */

public class ConsumerImpl implements Consumer{
	static CommandListReceiverDataBaseProxy commPort;
	static boolean ifConnect = false;
	static {
		if(PropertiesWS.BACKEND_IP.getString() != null){
			commPort = new CommandListReceiverDataBaseProxy();
			commPort.setEndpoint("http://"+PropertiesWS.BACKEND_IP.getString()+":"+ PropertiesWS.BAKCEND_PORT.getString() +"/SkypeBotBackend/services/CommandListReceiverDataBase");			
			ifConnect = true;
		}		
	}
	
	
	@Override
	public Object receive(String id) {
		return Cache.ifCacheElementExist(id) ? Cache.getCacheElement(id) : getAndCache(id);
	}
	
	private Object getAndCache(String id){
		try {
			if(ifConnect){
				String receiveString = commPort.receive(id);
				Cache.cacheObject(id, receiveString);
				return receiveString;
			}else return null;
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public void update(String id, Object Object) {
		try {
			if(ifConnect)
				commPort.update(id, XMLUtils.marshalCommandListString(Object));
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
}
