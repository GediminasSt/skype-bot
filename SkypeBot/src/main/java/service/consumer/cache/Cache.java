package service.consumer.cache;

import java.util.HashMap;
import java.util.Map;


public class Cache {
	private static final Map<String, Object> ObjectCache = new HashMap<String, Object>();
	
	public static void cacheObject(String id, Object object, long duration){
		synchronized (ObjectCache) {
			if(!ObjectCache.containsKey(id)){
				if(object != null){
					ObjectCache.put(id, object);
					new CacheTimer(id, duration*100);
				}
			}
		}
	}
	
	public static void cacheObject(String id, Object object){
		cacheObject(id,object,CacheConfig.DEFAULT_CACHE_TIME);
	}
	
	public static void clearCacheElement(String id) {
		synchronized (ObjectCache){
			ObjectCache.remove(id);
		}
	}
	public static Object getCacheElement(String id) {
		synchronized (ObjectCache){
			System.out.println("getting cached element");
			return ObjectCache.get(id);
		}
	}
	public static boolean ifCacheElementExist(String id) {
		synchronized (ObjectCache){
			return ObjectCache.containsKey(id);
		}
	}
	
}
