package org.skype.lt;

import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.skype.Chat;
import com.skype.Skype;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ChatHandler.class,Chat.class,Skype.class,Boolean.class})
public class ChatHandlerTest {
	
	public final String ID = "id";
	public final String CHECKED_CHAT_ID1 = "#someId1/id;123123123";
	public final String CHECKED_CHAT_ID2 = "#someId2/id;123123123";
	public final String CHECKED_CHAT_WINDOW_TITLE1 = "Text";
	public final String CHECKED_CHAT_WINDOW_TITLE2 = "Text,Text";
	public final String CHECKED_MESSAGE = "MEssage";
	@Test
	public void testpopulateSWTListWithChats1() throws Exception{
		PowerMock.mockStatic(Skype.class);
		Chat chatMock1 = PowerMock.createMock(Chat.class);
		Chat chatMock2 = PowerMock.createMock(Chat.class);
		org.eclipse.swt.widgets.List swtListMock = Mockito.mock(org.eclipse.swt.widgets.List.class);
		
		EasyMock.expect(chatMock1.getId()).andReturn(CHECKED_CHAT_ID1);
		EasyMock.expect(chatMock2.getId()).andReturn(CHECKED_CHAT_ID2);
		EasyMock.expect(Skype.getAllChats()).andReturn(new Chat[]{chatMock1,chatMock2});
		Mockito.doNothing().when(swtListMock).removeAll();
		EasyMock.expect(chatMock1.getWindowTitle()).andReturn(CHECKED_CHAT_WINDOW_TITLE1).times(3);
		EasyMock.expect(chatMock2.getWindowTitle()).andReturn(CHECKED_CHAT_WINDOW_TITLE2).times(4);
		PowerMock.replayAll();
		
		ChatHandler.populateSWTListWithChats(swtListMock,false);
		ChatHandler.populateSWTListWithChats(swtListMock,true);
		PowerMock.verifyAll();
	}		
}
