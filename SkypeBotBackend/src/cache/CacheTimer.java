package cache;

public class CacheTimer extends Thread{
	
	private String id;
	private long time;
	
	public CacheTimer(String id, long time) {
		this.id = id;
		this.time = time;		
		this.start();
	}
	
	@Override
	public void run() {
		try {
			Thread.sleep(time);
			Cache.clearCacheElement(id);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		super.run();
	}

}
