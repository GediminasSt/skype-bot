package mysqlconnections;

import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseConnection {
	public static Connection connection = null;
	public Connection getConnection() throws SQLException;
}
