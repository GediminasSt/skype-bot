package mysqlconnections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseConnectionImpl implements DatabaseConnection{
	
	public ResultSet getResultSet(String querry){
		try {
			return getConnection().createStatement().executeQuery(querry);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void updateDatabase(PreparedStatement statement){
		try {
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Connection getConnection() throws SQLException {
		if(connection == null){
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			return DriverManager.getConnection(
				DatabaseConnectionParameters.DB_SERVER_URL, 
				DatabaseConnectionParameters.DB_USER, 
				DatabaseConnectionParameters.DB_USER_PASSWORD);
		}else{
			return connection;
		}
	}

}
