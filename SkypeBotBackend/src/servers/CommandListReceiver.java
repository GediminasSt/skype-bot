package servers;


public interface CommandListReceiver {
	public String receive(String chatId);
	public void update(String chatId, String xmlObject);
}
