package servers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.jws.WebMethod;
import javax.jws.WebService;

import mysqlconnections.DatabaseConnectionImpl;
import mysqlconnections.DatabaseConnectionParameters;
import cache.Cache;

@WebService
public class CommandListReceiverDataBase extends CommandListReceiverImpl{
	
	@WebMethod
	@Override
	public String fetchCommandList(String chatId) {		
		return Cache.ifCacheElementExist(chatId) ? (String) Cache.getCacheElement(chatId) : fetchAndCacheFromDB(chatId);
	}
	
	private String fetchAndCacheFromDB(String chatId){
		try {
			DatabaseConnectionImpl connection = new DatabaseConnectionImpl();
			ResultSet commandListResultSet = 
					connection.getResultSet("SELECT commandlist FROM " + DatabaseConnectionParameters.DB_TABLE_NAME + " WHERE id='"+ chatId +"'");
			String commandListXmlStringFromDatabase = null;
			while(commandListResultSet.next()){
				commandListXmlStringFromDatabase = 
						commandListResultSet.getString("commandlist");
			}
			return commandListXmlStringFromDatabase;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@WebMethod
	@Override
	public void update(String chatId, String commandList) {
		DatabaseConnectionImpl connection = new DatabaseConnectionImpl();
		try {
			PreparedStatement preparedStatement = 
					connection.getConnection().prepareStatement("INSERT IGNORE INTO commandschema (id, commandlist) VALUES (?,?)");
			preparedStatement.setString(1, chatId);
			preparedStatement.setObject(2, commandList);
			connection.updateDatabase(preparedStatement);
			connection.getConnection().close();;
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}

}
