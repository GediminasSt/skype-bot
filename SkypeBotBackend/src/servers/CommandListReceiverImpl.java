package servers;


public abstract class CommandListReceiverImpl implements CommandListReceiver{

	@Override
	public String receive(String chatId) {
		return fetchCommandList(chatId);
	}
	
	abstract String fetchCommandList(String chatId);	
}
