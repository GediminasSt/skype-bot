/**
 * CommandListReceiverDataBase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servers;

public interface CommandListReceiverDataBase extends java.rmi.Remote {
    public void update(java.lang.String chatId, java.lang.String commandList) throws java.rmi.RemoteException;
    public java.lang.String receive(java.lang.String chatId) throws java.rmi.RemoteException;
}
