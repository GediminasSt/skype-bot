package servers;

public class CommandListReceiverDataBaseProxy implements servers.CommandListReceiverDataBase {
  private String _endpoint = null;
  private servers.CommandListReceiverDataBase commandListReceiverDataBase = null;
  
  public CommandListReceiverDataBaseProxy() {
    _initCommandListReceiverDataBaseProxy();
  }
  
  public CommandListReceiverDataBaseProxy(String endpoint) {
    _endpoint = endpoint;
    _initCommandListReceiverDataBaseProxy();
  }
  
  private void _initCommandListReceiverDataBaseProxy() {
    try {
      commandListReceiverDataBase = (new servers.CommandListReceiverDataBaseServiceLocator()).getCommandListReceiverDataBase();
      if (commandListReceiverDataBase != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)commandListReceiverDataBase)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)commandListReceiverDataBase)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (commandListReceiverDataBase != null)
      ((javax.xml.rpc.Stub)commandListReceiverDataBase)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servers.CommandListReceiverDataBase getCommandListReceiverDataBase() {
    if (commandListReceiverDataBase == null)
      _initCommandListReceiverDataBaseProxy();
    return commandListReceiverDataBase;
  }
  
  public void update(java.lang.String chatId, java.lang.String commandList) throws java.rmi.RemoteException{
    if (commandListReceiverDataBase == null)
      _initCommandListReceiverDataBaseProxy();
    commandListReceiverDataBase.update(chatId, commandList);
  }
  
  public java.lang.String receive(java.lang.String chatId) throws java.rmi.RemoteException{
    if (commandListReceiverDataBase == null)
      _initCommandListReceiverDataBaseProxy();
    return commandListReceiverDataBase.receive(chatId);
  }
  
  
}