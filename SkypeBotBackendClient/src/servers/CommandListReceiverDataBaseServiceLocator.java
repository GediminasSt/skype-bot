/**
 * CommandListReceiverDataBaseServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servers;

public class CommandListReceiverDataBaseServiceLocator extends org.apache.axis.client.Service implements servers.CommandListReceiverDataBaseService {

    public CommandListReceiverDataBaseServiceLocator() {
    }


    public CommandListReceiverDataBaseServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CommandListReceiverDataBaseServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CommandListReceiverDataBase
    private java.lang.String CommandListReceiverDataBase_address = "http://localhost:8080/SkypeBotBackend/services/CommandListReceiverDataBase";

    public java.lang.String getCommandListReceiverDataBaseAddress() {
        return CommandListReceiverDataBase_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CommandListReceiverDataBaseWSDDServiceName = "CommandListReceiverDataBase";

    public java.lang.String getCommandListReceiverDataBaseWSDDServiceName() {
        return CommandListReceiverDataBaseWSDDServiceName;
    }

    public void setCommandListReceiverDataBaseWSDDServiceName(java.lang.String name) {
        CommandListReceiverDataBaseWSDDServiceName = name;
    }

    public servers.CommandListReceiverDataBase getCommandListReceiverDataBase() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CommandListReceiverDataBase_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCommandListReceiverDataBase(endpoint);
    }

    public servers.CommandListReceiverDataBase getCommandListReceiverDataBase(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            servers.CommandListReceiverDataBaseSoapBindingStub _stub = new servers.CommandListReceiverDataBaseSoapBindingStub(portAddress, this);
            _stub.setPortName(getCommandListReceiverDataBaseWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCommandListReceiverDataBaseEndpointAddress(java.lang.String address) {
        CommandListReceiverDataBase_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (servers.CommandListReceiverDataBase.class.isAssignableFrom(serviceEndpointInterface)) {
                servers.CommandListReceiverDataBaseSoapBindingStub _stub = new servers.CommandListReceiverDataBaseSoapBindingStub(new java.net.URL(CommandListReceiverDataBase_address), this);
                _stub.setPortName(getCommandListReceiverDataBaseWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CommandListReceiverDataBase".equals(inputPortName)) {
            return getCommandListReceiverDataBase();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://servers", "CommandListReceiverDataBaseService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://servers", "CommandListReceiverDataBase"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CommandListReceiverDataBase".equals(portName)) {
            setCommandListReceiverDataBaseEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
