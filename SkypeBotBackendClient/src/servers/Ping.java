/**
 * Ping.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servers;

public interface Ping extends java.rmi.Remote {
    public java.lang.String pingMe() throws java.rmi.RemoteException;
}
