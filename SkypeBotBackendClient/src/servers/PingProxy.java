package servers;

public class PingProxy implements servers.Ping {
  private String _endpoint = null;
  private servers.Ping ping = null;
  
  public PingProxy() {
    _initPingProxy();
  }
  
  public PingProxy(String endpoint) {
    _endpoint = endpoint;
    _initPingProxy();
  }
  
  private void _initPingProxy() {
    try {
      ping = (new servers.PingServiceLocator()).getPing();
      if (ping != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)ping)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)ping)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (ping != null)
      ((javax.xml.rpc.Stub)ping)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servers.Ping getPing() {
    if (ping == null)
      _initPingProxy();
    return ping;
  }
  
  public java.lang.String pingMe() throws java.rmi.RemoteException{
    if (ping == null)
      _initPingProxy();
    return ping.pingMe();
  }
  
  
}