/**
 * PingService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servers;

public interface PingService extends javax.xml.rpc.Service {
    public java.lang.String getPingAddress();

    public servers.Ping getPing() throws javax.xml.rpc.ServiceException;

    public servers.Ping getPing(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
