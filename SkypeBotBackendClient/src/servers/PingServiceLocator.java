/**
 * PingServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servers;

public class PingServiceLocator extends org.apache.axis.client.Service implements servers.PingService {

    public PingServiceLocator() {
    }


    public PingServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PingServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for Ping
    private java.lang.String Ping_address = "http://localhost:8080/SkypeBotBackend/services/Ping";

    public java.lang.String getPingAddress() {
        return Ping_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PingWSDDServiceName = "Ping";

    public java.lang.String getPingWSDDServiceName() {
        return PingWSDDServiceName;
    }

    public void setPingWSDDServiceName(java.lang.String name) {
        PingWSDDServiceName = name;
    }

    public servers.Ping getPing() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(Ping_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPing(endpoint);
    }

    public servers.Ping getPing(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            servers.PingSoapBindingStub _stub = new servers.PingSoapBindingStub(portAddress, this);
            _stub.setPortName(getPingWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPingEndpointAddress(java.lang.String address) {
        Ping_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (servers.Ping.class.isAssignableFrom(serviceEndpointInterface)) {
                servers.PingSoapBindingStub _stub = new servers.PingSoapBindingStub(new java.net.URL(Ping_address), this);
                _stub.setPortName(getPingWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("Ping".equals(inputPortName)) {
            return getPing();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://servers", "PingService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://servers", "Ping"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("Ping".equals(portName)) {
            setPingEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
